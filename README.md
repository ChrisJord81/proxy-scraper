# Proxy Scraper

Scrapes Proxies. Automatic self-sustaining script that will dump, check, and maintain proxies into a database.
Runs continuously without user input.

## Proxy Source List
*  
*
*
*


## The Generation Process
1.  Every 24 hours or whenever the script runs out of proxies, the proxy 
    scraping code will put unchecked proxies into the database. These proxies
    are at the tester's highest priority above already checked proxies. 
2.  The Script will order the proxies from high priority to low by testing in this order
    * Unchecked
    * Last Checked
    * Bad
    * Good
3.  The script will continue until the user stops it.


## The Table Structure

| Proxy | Port | Type | Anonymity | Working | Speed | Location | Sites | ISP |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| x.x.x.x | xx | socks/https | 1,2, or 3 | true or false | time in seconds | Country, City | site1,site2 | isp |









